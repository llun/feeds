module.exports = {
  mode: 'jit',
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './lib/components/**/*.{js,ts,jsx,tsx}',
    './lib/utils.{js,ts,jsx,tsx}'
  ],
  plugins: [require('@tailwindcss/typography')]
}
